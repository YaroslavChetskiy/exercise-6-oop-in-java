public class Main {
    public static void main(String[] args) {
        Category category1 = new Category("Category1", new Product[] {
                new Product("product1", 22.1, 5.0),
                new Product("product2", 45.3, 4.9)
        });

        Category category2 = new Category("Category2", new Product[] {
                new Product("product3", 35.12, 1.2),
                new Product("product4", 100, 2.0)
        });

        User user1 = new User("User1", "qwerty", new Basket(category1.getProducts()));
    }
}
